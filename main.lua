-- Abstract: mouseHover Library Plugin Demo

--[[
--Uncomment this block to demo the unRequire() method

 local mouseHover = require "plugin.mouseHover"
	
	print("before unRequire", package.loaded["plugin.mouseHover"])

	mouseHover.unRequire()
	mouseHover = nil

	print("after unRequire", package.loaded["plugin.mouseHover"])
]]--

local mouseHover = require 'plugin.mouseHover' -- the plugin is activated by default. 

--[[
-- Uncomment this block to demo the deactivate() and activate() methods

	timer.performWithDelay(3000, mouseHover.deactivate)

	timer.performWithDelay(6000, mouseHover.activate)


]]--



local halfW = display.contentWidth * 0.5
local halfH = display.contentHeight * 0.5

local defaultAlpha = 0.4




local bkg = display.newRect(halfW,halfH,halfW*2,halfH*2 )
bkg.fill = {1,1,1,0.7}

local onMouseHover = function(event)
	if event.phase == "began" then
		event.target.isVisible = true
		event.target.alpha = 1
		

	elseif event.phase == "ended" then

		event.target.alpha = defaultAlpha

	end

	-- not returning true

end



local widget = require("widget")


--[[local onOffSwitch = widget.newSwitch(
    {
        left = halfW*0.5,
        top = halfH*1.7,
        width = halfW*0.2,
        height = halfW*0.1,
        style = "onOff",
        id = "onOffSwitch",
        isAnimated = true,
    }
)

onOffSwitch.rotation = 50
onOffSwitch.xScale = 3
onOffSwitch.yScale = 3

onOffSwitch.alpha = defaultAlpha
onOffSwitch:addEventListener( "mouseHover", onMouseHover)


local button1 = widget.newButton(
    {
        label = "button",
        
        emboss = false,
        -- Properties for a rounded rectangle button
        shape = "roundedRect",
        width = 200,
        height = 40,
        cornerRadius = 2,
        fillColor = { default={1,0,0,1}, over={1,0.1,0.7,0.4} },
        strokeColor = { default={1,0.4,0,1}, over={0.8,0.8,1,1} },
        strokeWidth = 4
    }
)

button1.xScale = 3
button1.yScale = 3

button1.rotation = -45
-- Center the button
button1.x = halfW*0.4
button1.y = halfH*0.7
button1.alpha = defaultAlpha
button1:addEventListener( "mouseHover", onMouseHover)

local text = display.newText("Hello World", halfW*1.4, halfH*0.4,0,0, native.systemFont, 100)
text.fill = {1,0,0}

text.rotation = 30
text.xScale = 1.5
text.yScale = 0.7

text.anchorX = 1

text.alpha = defaultAlpha
text:addEventListener( "mouseHover", onMouseHover)]]--

local vertices = { 100,-110, 127,-35, 205,-35, 143,16, 165,90, 80,5, 100-65,90, 100-43,15, 100-105,-35, 100-27,-35}
 
local o = display.newPolygon( halfW, halfH, vertices )
o.fill = {0.2,0.2,0.7}
o.alpha = defaultAlpha
o.xScale = 6
o.yScale = 4

o.anchorX =1

o.rotation = 0
o:addEventListener( "mouseHover", onMouseHover )

local rect = display.newRoundedRect(halfW*1, halfH*0.7, halfW*0.2, halfW*0.3, halfW*0.05)
rect.rotation = 0
rect.fill = {1,0.3,0.4}
rect.xScale = 2.5
rect.yScale = 2.5
rect.alpha = defaultAlpha
rect.anchorY = 0
rect:addEventListener( "mouseHover", onMouseHover )


local circle = display.newCircle(halfW*1.2, halfH*1.2, halfW*0.1)
circle.rotation = 30
circle.fill = {0,0.3,0.4}
circle.alpha = defaultAlpha
circle.xScale = 5
circle.yScale = 1.5
circle.anchorX = 1
circle.anchorY = 1
circle:addEventListener( "mouseHover", onMouseHover )

local onCircHover = function(event)

	return true
end


circle:addEventListener( "mouseHover", onCircHover )


local testGroup = display.newGroup()
testGroup.anchorChildren = true
testGroup.x = halfW*1.2
testGroup.y = halfH*1.2
testGroup:insert(rect)
testGroup:insert(circle)

testGroup.anchorX = 0
local groupHoverListener = function(event)

	--print("groupHover", event.phase)

end
testGroup:addEventListener( "mouseHover", groupHoverListener )



--mouseHover.setScope(testGroup) -- Uncomment this to see how the scope of Mouse Hover can be changed

-------------------------------------------------------------------------------
-- END
-------------------------------------------------------------------------------]]--
